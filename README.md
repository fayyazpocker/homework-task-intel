## Algorithm

### Functions and Output 
The function, *IdenticallyColoredConnectedComponents()* is added in *TriangleMesh.cpp* as **list<set<<int>int> TriangleMesh::IdenticallyColoredConnectedComponents()**. The corresponding function declaration is added to *TriangleMesh.h*.
The output or the return type of the function is of the type **list<set<<int>int>>**.

The Algorithm have two parts : 
- **Setting compare and vertex flags** for reducing the number of colour comparison based upon the vertex that are already present in the list. 
 For instance, a vertex_flag of [1, 0, 1] indicates that the 0th and 2nd vertices of the triangles are already present in the output list 
 and 1st vertex of the triangle is not present. Hence if 1st vertex colour does not meet the colors of 0th or 2nd vertices, 
 new set with that corresponding vertex will be added to the list. Similiarly, a compare flag of [1, 1, 0] indicates that the 2nd vertex is already present in the set same as that of 1st vertex 
 and hence no comparison between them is required. Only 0th and 1st vertices will be compared.
- **Colour comparison of flagged vertices** involves comparing the colours of the vertices which are necessary. Vertices whose colours are already been compared 
 and are present in the same set will not be compared again. When colour match between two vertices happen which already have a set associated with it, the set with less size will be 
added to the set with a higher size and the small set will be cleared.

The algorithm is represented as per the following flowcharts:

#### Summary of algorithm

```mermaid
graph LR
A(Start)-->B{triangle < Total no of triangles}
B-->|True| C[Setting vertex and compare flag]
style C fill:#f9f,stroke:#333,stroke-width:4px
C-->D[Comparing colors of flagged vertices]
style D fill:#f90,stroke:#333,stroke-width:4px
D-->F(triangles++)
F-->B
B-->|False| G[Sort the output list]
G--> H[Remove all null sets]
H--> I[Return output]
```

#### Setting vertex and compare flag
```mermaid
graph LR
A{vertex < 3}
A-->|False| B{vertex_flag of 0th vertex}
B-->|False| C[Create new set]
C-->D[Push to output list]

A-->|True| E{vertex in output list}
E-->|True| F[vertex_flag set to true]
F--> G{Other vertices in the same set}
G-->|True| J[Compare flag set to false]

E-->|False| I[vertex++]
G-->|False| I
I -->A
```

#### Comparing colors of flagged vertices
```mermaid
graph LR
A{vertex < 3}
A-->|True| B{compare_flag}
B-->|True| C{Compare color}
C-->|True| E{vertex_flag}
E-->|True| F[cut the entire set into the matched set]
E-->|False| G[append the vertex to the set]
C-->|False| H{vertex_flag}
H-->|False| I[add new set]
H-->|True| L[vertex++]
L --> A
F-->L
G-->L
I-->L
```
