# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/fayyaz/intel/recurse/Open3D/3rdparty/tinyobjloader/tiny_obj_loader.cc" "/home/fayyaz/intel/recurse/Open3D/3rdparty/CMakeFiles/tinyobjloader.dir/tinyobjloader/tiny_obj_loader.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "UNIX"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "3rdparty/tinyobjloader"
  "3rdparty/qhull/src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
