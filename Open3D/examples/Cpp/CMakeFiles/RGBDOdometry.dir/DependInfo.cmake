# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/fayyaz/intel/recurse/Open3D/examples/Cpp/RGBDOdometry.cpp" "/home/fayyaz/intel/recurse/Open3D/examples/Cpp/CMakeFiles/RGBDOdometry.dir/RGBDOdometry.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "UNIX"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src"
  "3rdparty"
  "3rdparty/Eigen"
  "3rdparty/flann"
  "3rdparty/flann/algorithms"
  "3rdparty/flann/nn"
  "3rdparty/flann/util"
  "3rdparty/glew/include"
  "3rdparty/GLFW/include"
  "/usr/include/jsoncpp"
  "3rdparty/liblzf"
  "3rdparty/tomasakeninemoeller"
  "3rdparty/libpng"
  "3rdparty/rply"
  "3rdparty/rply/etc"
  "3rdparty/rply/manual"
  "3rdparty/tinyfiledialogs"
  "3rdparty/tinygltf"
  "3rdparty/tinygltf/cmake"
  "3rdparty/tinygltf/deps"
  "3rdparty/tinygltf/examples"
  "3rdparty/tinygltf/examples/basic"
  "3rdparty/tinygltf/examples/basic/basic"
  "3rdparty/tinygltf/examples/common"
  "3rdparty/tinygltf/examples/common/OpenGLWindow"
  "3rdparty/tinygltf/examples/common/ThirdPartyLibs"
  "3rdparty/tinygltf/examples/common/ThirdPartyLibs/Glew"
  "3rdparty/tinygltf/examples/common/ThirdPartyLibs/Glew/CustomGL"
  "3rdparty/tinygltf/examples/common/glm"
  "3rdparty/tinygltf/examples/common/glm/cmake"
  "3rdparty/tinygltf/examples/common/glm/doc"
  "3rdparty/tinygltf/examples/common/glm/doc/api"
  "3rdparty/tinygltf/examples/common/glm/doc/api/search"
  "3rdparty/tinygltf/examples/common/glm/doc/manual"
  "3rdparty/tinygltf/examples/common/glm/doc/theme"
  "3rdparty/tinygltf/examples/common/glm/glm"
  "3rdparty/tinygltf/examples/common/glm/glm/detail"
  "3rdparty/tinygltf/examples/common/glm/glm/gtc"
  "3rdparty/tinygltf/examples/common/glm/glm/gtx"
  "3rdparty/tinygltf/examples/common/glm/glm/simd"
  "3rdparty/tinygltf/examples/common/glm/test"
  "3rdparty/tinygltf/examples/common/glm/test/bug"
  "3rdparty/tinygltf/examples/common/glm/test/core"
  "3rdparty/tinygltf/examples/common/glm/test/external"
  "3rdparty/tinygltf/examples/common/glm/test/external/gli"
  "3rdparty/tinygltf/examples/common/glm/test/external/gli/core"
  "3rdparty/tinygltf/examples/common/glm/test/gtc"
  "3rdparty/tinygltf/examples/common/glm/test/gtx"
  "3rdparty/tinygltf/examples/common/glm/util"
  "3rdparty/tinygltf/examples/common/glm/util/conan-package"
  "3rdparty/tinygltf/examples/common/glm/util/conan-package/lib_licenses"
  "3rdparty/tinygltf/examples/common/glm/util/conan-package/test_package"
  "3rdparty/tinygltf/examples/common/imgui"
  "3rdparty/tinygltf/examples/common/nativefiledialog"
  "3rdparty/tinygltf/examples/common/nativefiledialog/screens"
  "3rdparty/tinygltf/examples/common/nativefiledialog/src"
  "3rdparty/tinygltf/examples/common/nativefiledialog/src/include"
  "3rdparty/tinygltf/examples/common/nativefiledialog/test"
  "3rdparty/tinygltf/examples/gltfutil"
  "3rdparty/tinygltf/examples/glview"
  "3rdparty/tinygltf/examples/glview/cmake"
  "3rdparty/tinygltf/examples/glview/glview"
  "3rdparty/tinygltf/examples/raytrace"
  "3rdparty/tinygltf/examples/raytrace/images"
  "3rdparty/tinygltf/examples/saver"
  "3rdparty/tinygltf/examples/validator"
  "3rdparty/tinygltf/examples/validator/app"
  "3rdparty/tinygltf/examples/validator/src"
  "3rdparty/tinygltf/experimental"
  "3rdparty/tinygltf/models"
  "3rdparty/tinygltf/models/BoundsChecking"
  "3rdparty/tinygltf/models/Cube"
  "3rdparty/tinygltf/models/Extensions-issue97"
  "3rdparty/tinygltf/tests"
  "3rdparty/tinygltf/tools"
  "3rdparty/tinygltf/tools/windows"
  "3rdparty/qhull/src"
  "3rdparty/fmt/include"
  "3rdparty/librealsense/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/CMakeFiles/Open3D.dir/DependInfo.cmake"
  "/home/fayyaz/intel/recurse/Open3D/3rdparty/glew/CMakeFiles/glew.dir/DependInfo.cmake"
  "/home/fayyaz/intel/recurse/Open3D/3rdparty/GLFW/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  "/home/fayyaz/intel/recurse/Open3D/3rdparty/libpng/CMakeFiles/png.dir/DependInfo.cmake"
  "/home/fayyaz/intel/recurse/Open3D/3rdparty/zlib/CMakeFiles/zlib.dir/DependInfo.cmake"
  "/home/fayyaz/intel/recurse/Open3D/3rdparty/tinyfiledialogs/CMakeFiles/tinyfiledialogs.dir/DependInfo.cmake"
  "/home/fayyaz/intel/recurse/Open3D/3rdparty/CMakeFiles/tinyobjloader.dir/DependInfo.cmake"
  "/home/fayyaz/intel/recurse/Open3D/3rdparty/CMakeFiles/qhullcpp.dir/DependInfo.cmake"
  "/home/fayyaz/intel/recurse/Open3D/3rdparty/CMakeFiles/qhullstatic_r.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
