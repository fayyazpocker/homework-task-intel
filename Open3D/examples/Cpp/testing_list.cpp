#include <iostream>
#include <list>
#include <set>

using namespace std;

int main(){

	list <set<int> > ls;

	set<int > test[3];

	test[0].insert(5);
	test[0].insert(9);
	test[0].insert(10);

	test[1].insert(0);
	test[1].insert(2);
	test[1].insert(1);

	test[2].insert(7);
	test[2].insert(3);

	ls.push_back(test[0]);
	ls.push_back(test[1]);
	ls.push_back(test[2]);

	list<set<int> >::iterator out_itr,copy_out,copy_into;
	set<int >:: iterator in_itr;

	// ls.sort();

	cout<<"Before copying"<<endl;
	for (out_itr = ls.begin();out_itr != ls.end(); out_itr++)
	{
		cout<<"[";
		for(in_itr = out_itr->begin(); in_itr != out_itr->end(); in_itr++){
			cout <<*in_itr<<" "; 
		}
		cout<<"]"<<endl;
	}

	for (out_itr = ls.begin();out_itr != ls.end(); out_itr++)
	{
		for(in_itr = out_itr->begin(); in_itr != out_itr->end(); in_itr++){
			if (*in_itr==3){
				copy_out = out_itr;
			}
			if (*in_itr==0){
				copy_into = out_itr;
			}
		}
	}

	// list2.assign(list1.begin(),list1.end())
	copy_into->insert(copy_out->begin(),copy_out->end());
	copy_out->clear();

	ls.sort();
	cout<<"After copying"<<endl;
	for (out_itr = ls.begin();out_itr != ls.end(); out_itr++)
	{
		cout<<"[";
		for(in_itr = out_itr->begin(); in_itr != out_itr->end(); in_itr++){
			cout <<*in_itr<<" "; 
		}
		cout<<"]"<<endl;
	}

}