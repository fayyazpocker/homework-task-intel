#include <Eigen/Dense>
#include <initializer_list>
#include <iostream>
#include <typeinfo>
#include <set>
#include <vector>  // for 2D vector
#include <list>
using namespace std;

	list<set<int>> output;
	set<int>::iterator sub_itr;
	list<set<int>>::iterator out_itr, vertex_index[3];

void print_output()
{
		cout<<"[ ";
	for(out_itr=output.begin();out_itr!=output.end();out_itr++){
		cout<<"[ ";
		for (sub_itr = out_itr->begin(); sub_itr != out_itr->end(); sub_itr++)
			cout << *sub_itr<< " ";
		cout <<"]";
	}
	cout<<" ]"<<endl;

}

int main() {
	/*
	color
	  R G B
	0 1 0 0
	1 1 0 0
	2 0 0 1
	3 0 0 1
	4 0 1 0
	5 1 0 0   
	6 1 0 0
	*/
	vector<Eigen::Vector3d> vertex_color;
	// vector<vector<double>> vertex_color;
	Eigen::Vector3d v0(1, 0, 0);
	Eigen::Vector3d v1(1, 0, 0);
	Eigen::Vector3d v2(1, 0, 0);
	Eigen::Vector3d v3(0, 1, 0);
	Eigen::Vector3d v4(0, 1, 0);
	Eigen::Vector3d v5(0, 1, 0);
	Eigen::Vector3d v6(0, 0, 1);
	Eigen::Vector3d v7(0, 0, 1);
	Eigen::Vector3d v8(0, 0, 1);

	vertex_color.push_back(v0);
	vertex_color.push_back(v1);
	vertex_color.push_back(v2);
	vertex_color.push_back(v3);
	vertex_color.push_back(v4);
	vertex_color.push_back(v5);
	vertex_color.push_back(v6);
	vertex_color.push_back(v7);
	vertex_color.push_back(v8);

	cout << "COLORS" << endl;
	cout << "[ " << endl;
	for (unsigned int i = 0; i < vertex_color.size(); i++)
		cout << " [ " << vertex_color[i][0] << " " << vertex_color[i][1] << " "<< vertex_color[i][2] << " ]" << endl;
	cout << "] " << endl;

	/*
	triangles
	3 0 1 3
	3 1 3 4
	3 4 3 6
	3 3 6 5
	3 2 3 5
	3 0 2 3
	*/
	vector<Eigen::Vector3i> triangles;

	Eigen::Vector3i t0(0, 1, 2);
	Eigen::Vector3i t1(3, 4, 5);
	Eigen::Vector3i t2(6, 7, 8);
	Eigen::Vector3i t3(2, 3, 6);
	// Eigen::Vector3i t4(2, 3, 5);
	// Eigen::Vector3i t5(0, 2, 3);

	//Testing added
	// Eigen::Vector3i t4(5, 7, 56);


	triangles.push_back(t0);
	triangles.push_back(t1);
	triangles.push_back(t2);
	triangles.push_back(t3);
	// triangles.push_back(t4);
	// triangles.push_back(t5);

	cout << "TRIANGLES" << endl;
	cout << "[ " << endl;
	for (unsigned int i = 0; i < triangles.size(); i++)
		cout << " [ " << triangles[i][0] << " " << triangles[i][1] << " "<< triangles[i][2] << " ]" << endl;
	cout << "] " << endl;

	/*
		output
	[[0, 1], [2, 3], [4], [5, 6]]
	*/
	cout << "OUTPUT" << endl;

	// vector<vector<int>> output;
	// cout << v0.cwiseProduct(v1) << endl;

	////test_cases for output
	bool vertex_flag[3];
	bool compare_flag[3];
	// list<set<int>>::iterator vertex_index[3];

	// set<int> test_case;
	// test_case.insert(0);
	// test_case.insert(1);
	// test_case.insert(6);

	// set<int> test_case_1;
	// test_case_1.insert(3);
	// test_case_1.insert(4);

	// output.push_back(test_case);
	// output.push_back(test_case_1);

////////////////////// COMPARING FACES ONE BY ONE
	for (unsigned int face = 0; face < triangles.size(); face++) {
		fill(begin(vertex_flag), end(vertex_flag), false);   // all vertex flag false
		fill(begin(compare_flag), end(compare_flag), true);  // all compare flag true


////////////////////FOR LOOP FOR SETTING FLAGS
		// Find if the vertices are already present in the output list or not
		for (unsigned int vertex = 0; vertex < triangles[face].size(); vertex++){
			if (!vertex_flag[vertex]){  // if vertex is already present no need to search
				for (out_itr = output.begin(); out_itr != output.end();out_itr++){  // traversing though each set inside output set
					if (out_itr->find(triangles[face][vertex]) != out_itr->end()) {  // if vertex if found
						vertex_flag[vertex] = true;
						vertex_index[vertex] = out_itr; 
						// checking if the next vertex is present inside the same set to avoid searching in other list
						if (vertex < 2)
							for (unsigned int i = vertex + 1; i < 3; i++)
								if (out_itr->find(triangles[face][i]) != out_itr->end()) {  // other vertex_i is also present in the same list
									vertex_flag[i] = true;
									compare_flag[i] = false;
									vertex_index[i] = out_itr;
								}
						break;
					}
				}
				if(!vertex_flag[vertex])
				{
					set<int> new_sub;
					new_sub.insert(triangles[face][vertex]);
					output.push_back(new_sub);
					list<set<int> >::iterator last_itr = output.end();
					// set<int>::iterator sub_itr;
					vertex_flag[vertex] = true;
					vertex_index[vertex] = --last_itr;
				}
			}
			print_output();
		}
/////////////////////////////////END OF COMPUTING FLAGS

		// Comparing colours of the vertices based on the flags
		// if(!vertex_flag[0])
		// {
		// 	set<int> new_sub;
		// 	new_sub.insert(triangles[face][0]);
		// 	output.push_back(new_sub);
		// 	// set<int>::iterator sub_itr;
		// 	vertex_flag[0] = true;
		// 	// sub_itr = new_sub.begin();
		// 	// vertex_index[0] = new_sub.begin();		
		// }
		// cout<<typeid(vertex_index).name();
			// for(out_itr=vertex_index[0].begin();out_itr!=vertex_index[0].end();out_itr++){
			// 	cout<<"[";

		// cout << "vertex_flag: [ " << vertex_flag[0] << " " << vertex_flag[1] << " " << vertex_flag[2] << " ]"<<endl;
		// cout << "compare_flag: [ " << compare_flag[0] << " " << compare_flag[1] << " " << compare_flag[2] << " ]"<<endl;

cout<<"COLOR COMPARISON"<<endl;
		cout << "vertex_flag: [ " << vertex_flag[0] << " " << vertex_flag[1] << " " << vertex_flag[2] << " ]"<<endl;
		cout << "compare_flag: [ " << compare_flag[0] << " " << compare_flag[1] << " " << compare_flag[2] << " ]"<<endl;
////////////////////////////COMPARING COLOURS BASED ON FLAGS
		for (unsigned int vertex = 1; vertex < triangles[face].size(); vertex++){
			if(compare_flag[vertex]){ //if compare is set it to true
				if (((vertex_color[triangles[face][vertex-1]] + vertex_color[triangles[face][vertex]]) * 0.5).isApprox(vertex_color[triangles[face][vertex-1]])){
					if(vertex_flag[vertex]){
						//already there is a sublist for the corresponding vertex
						vertex_index[vertex-1]->insert(vertex_index[vertex]->begin(),vertex_index[vertex]->end());
						vertex_index[vertex]->clear(); //clearing the 
						output.sort();
						output.erase(output.begin());
						// cout<<"match and vertex present"<<endl;
					}
					else{
						set<int> new_sub;
						new_sub.insert(triangles[face][vertex]);
						output.push_back(new_sub);
					}
				}
				else{
					if(vertex_flag[vertex])
						continue;
					else{
						set<int> new_sub;
						new_sub.insert(triangles[face][vertex]);
						output.push_back(new_sub);
					}
				}
			}
			//Compare 0th and 2nd element if 1st element is not equal to 0th element
			if(compare_flag[1] && compare_flag[2]){
				//ie 111 or 100..this requires comparison of 0th and 2nd element also
				if (((vertex_color[triangles[face][0]] + vertex_color[triangles[face][2]]) * 0.5).isApprox(vertex_color[triangles[face][0]])){
					if(vertex_flag[2]){
						//already there is a sublist for the corresponding vertex
						vertex_index[0]->insert(vertex_index[2]->begin(),vertex_index[2]->end());
						vertex_index[2]->clear();
						// cout<<"match and vertex present"<<endl;
					}
					else{
						set<int> new_sub;
						new_sub.insert(triangles[face][2]);
						output.push_back(new_sub);
					}
				}
				else{
					if(vertex_flag[2])
						continue;
					else{
						set<int> new_sub;
						new_sub.insert(triangles[face][2]);
						output.push_back(new_sub);
					}
				}
			}
			print_output();
		}

	////////////////////////////////////////////////END OF COLOUR COMPARISION

		// }

//Output of OUTPUT
		cout<<"VERTEX OUTPUT"<<endl;
		output.sort();
		print_output();

	}

/////////////////////////////////END OF FACE COMARISON 
}

		/*

				// (0 == 1)
				if (((vertex_color[triangles[face][0]] +
					  vertex_color[triangles[face][1]]) *
					 0.5)
							.isApprox(vertex_color[triangles[face][0]])) {
					// (0 == 1) && (0 == 2)
					if (((vertex_color[triangles[face][0]] +
						  vertex_color[triangles[face][2]]) *
						 0.5)
								.isApprox(vertex_color[triangles[face][0]])) {
						// [0 1 2]
						cout << "v" << triangles[face][0] << " == "
							 << "v" << triangles[face][1] << " == "
							 << "v" << triangles[face][2] << endl;
						// (0 == 1) && (0 != 2)
					} else {
						// [0 1][2]
						cout << "v" << triangles[face][0] << " == "
							 << "v" << triangles[face][1] << endl;
						// pair<bool, int> result = indexinvect<int>(vecOfNums,
						// 45);
						// if (result.first)
						//     std::cout << "Element Found at index : " <<
						//     result.second
						//     << std::endl;
						// else
						//     std::cout << "Element Not Found" << std::endl;
					}
					// (0 != 1)
				} else {
					// (0 != 1) && (0 == 2)
					if (((vertex_color[triangles[face][0]] +
						  vertex_color[triangles[face][2]]) *
						 0.5)
								.isApprox(vertex_color[triangles[face][0]])) {
						//[0 2][1]
						cout << "v" << triangles[face][0] << " == "
							 << "v" << triangles[face][2] << endl;
					} else {
						// (0 != 1) && (0 != 2) && (1 == 2)
						if (((vertex_color[triangles[face][1]] +
							  vertex_color[triangles[face][2]]) *
							 0.5)
									.isApprox(vertex_color[triangles[face][1]]))
		   {
							//[0] [1 2]
							cout << "v" << triangles[face][1] << " == "
								 << "v" << triangles[face][2] << endl;
						}
						// (0 != 1) && (0 != 2) && (1 != 2)
						else {
							//[0][1][2]
							cout << "v" << triangles[face][0] << " != "
								 << "v" << triangles[face][1] << " !="
								 << "v" << triangles[face][2] << endl;
						}
					}
				}
		*/
	// }
	// cout << "vertex_flag: [ " << vertex_flag[0] << " " << vertex_flag[1] << " " << vertex_flag[2] << " ]"<<endl;
	// cout << "compare_flag: [ " << compare_flag[0] << " " << compare_flag[1] << " " << compare_flag[2] << " ]"<<endl;
	// cout << "index_iterator: [ " << vertex_index[0] << " " << vertex_index[1] << " " << vertex_index[2] << " ]";
// }
