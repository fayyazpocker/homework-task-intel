#include "Open3D/Open3D.h"
#include <Eigen/Dense>
#include <initializer_list>
#include <iostream>
#include <typeinfo>
#include <set>
#include <vector>  // for 2D vector
#include <list>
using namespace open3d;
using namespace std;

    list<set<int>> output;
    set<int>::iterator sub_itr;
    list<set<int>>::iterator out_itr, vertex_index[3];

void print_output()
{
        cout<<"[ ";
    for(out_itr=output.begin();out_itr!=output.end();out_itr++){
        cout<<"[ ";
        for (sub_itr = out_itr->begin(); sub_itr != out_itr->end(); sub_itr++)
            cout << *sub_itr<< " ";
        cout <<"]";
    }
    cout<<" ]"<<endl;

}


int main() {
    // Read triangle mesh "test_mesh.ply"
    geometry::TriangleMesh mesh;
    // io::ReadTriangleMesh("../../examples/TestData/test_1.ply", mesh, true);

    if (io::ReadTriangleMesh(
                "../../../examples/TestData/assignment_test/test_half_5.ply",
                mesh)) {
        utility::LogInfo("Successfully read {}\n", "test_1.ply");
    } else {
        utility::LogError("Failed to read {}\n\n", "test_1.ply");
        return 1;
    }

    mesh.ComputeVertexNormals();
    auto mesh_ptr = std::make_shared<geometry::TriangleMesh>();
    *mesh_ptr = mesh;
    // visualization::DrawGeometries({mesh_ptr}, "Mesh", 1600, 900);

    // Then get connected components
    // mesh.IdenticallyColoredConnectedComponents();

    std::cout << "vertices: " << mesh.vertices_.size() << std::endl;
    std::cout << "triangles: " << mesh.triangles_.size() << std::endl;

    // std::cout << "VERTICES : COLOR" << std::endl;
    // for (unsigned int i = 0; i < mesh.vertices_.size(); i++) {
    //     std::cout << "[ " << mesh.vertices_[i][0] << " " << mesh.vertices_[i][1]
    //               << " " << mesh.vertices_[i][2] << " "
    //               << mesh.vertex_colors_[i][0] << " "
    //               << mesh.vertex_colors_[i][1] << " "
    //               << mesh.vertex_colors_[i][2] << " ]" << std::endl;
    // }


    std::cout << "COLOR" << std::endl;
    for (unsigned int i = 0; i < mesh.vertex_colors_.size(); i++) {
        std::cout <<i<< " [ " << mesh.vertex_colors_[i][0] << " "
                  << mesh.vertex_colors_[i][1] << " "
                  << mesh.vertex_colors_[i][2] << " ]" << std::endl;
    }

    std::cout << "TRIANGLES" << std::endl;
    for (unsigned int i = 0; i < mesh.triangles_.size(); i++) {
        std::cout << "[ " << mesh.triangles_[i][0] << " "
                  << mesh.triangles_[i][1] << " " << mesh.triangles_[i][2]
                  << " ]" << std::endl;
    }


    bool vertex_flag[3];
    bool compare_flag[3];
    int no_color_comp=0;
    int prev_color_comp=0;
    int no_of_null_set=0;

////////////////////// COMPARING FACES ONE BY ONE
    for (unsigned int face = 0; face < mesh.triangles_.size(); face++) {
        fill(begin(vertex_flag), end(vertex_flag), false);   // all vertex flag false
        fill(begin(compare_flag), end(compare_flag), true);  // all compare flag true


////////////////////FOR LOOP FOR SETTING FLAGS
        // Find if the vertices are already present in the output list or not
        for (unsigned int vertex = 0; vertex < mesh.triangles_[face].size(); vertex++){
            if (!vertex_flag[vertex]){  // if vertex is already present no need to search
                for (out_itr = output.begin(); out_itr != output.end();out_itr++){  // traversing though each set inside output set
                    if (out_itr->find(mesh.triangles_[face][vertex]) != out_itr->end()) {  // if vertex if found
                        vertex_flag[vertex] = true;
                        vertex_index[vertex] = out_itr; 
                        // checking if the next vertex is present inside the same set to avoid searching in other list
                        if (vertex < 2)
                            for (unsigned int i = vertex + 1; i < 3; i++)
                                if (out_itr->find(mesh.triangles_[face][i]) != out_itr->end()) {  // other vertex_i is also present in the same list
                                    vertex_flag[i] = true;
                                    compare_flag[i] = false;
                                    vertex_index[i] = out_itr;
                                }
                        break;
                    }
                }
                // if(!vertex_flag[vertex])
                // {
                //     set<int> new_sub;
                //     new_sub.insert(mesh.triangles_[face][vertex]);
                //     output.push_back(new_sub);
                //     list<set<int> >::iterator last_itr = output.end();
                //     // set<int>::iterator sub_itr;
                //     vertex_flag[vertex] = true;
                //     vertex_index[vertex] = --last_itr;
                // }
            }
            cout<<"Out of flag loop"<<endl;
            print_output();
            getchar();
        }
/////////////////////////////////END OF COMPUTING FLAGS



//////////////////////Adding a new element if first vertex is not added

                if(!vertex_flag[0])
                {
                    set<int> new_sub;
                    new_sub.insert(mesh.triangles_[face][0]);
                    output.push_back(new_sub);
                    list<set<int> >::iterator last_itr = output.end();
                    // set<int>::iterator sub_itr;
                    vertex_flag[0] = true;
                    vertex_index[0] = --last_itr;

                    cout<<"After adding new first element if not present"<<endl;
                    print_output();
                    getchar();
                }



//////////////////////////End of adding first vertex



////////////////////////////COMPARING COLOURS BASED ON FLAGS
        for (unsigned int vertex = 1; vertex < mesh.triangles_[face].size(); vertex++){
            cout<<"To color comp loop"<<endl;
            cout << "vertex_flag: [ " << vertex_flag[0] << " " << vertex_flag[1] << " " << vertex_flag[2] << " ]"<<endl;
            cout << "compare_flag: [ " << compare_flag[0] << " " << compare_flag[1] << " " << compare_flag[2] << " ]"<<endl;
            print_output();
            getchar();
            if(compare_flag[vertex]){ //if compare is set it to true
                no_color_comp++;
                //No need to compare the vertices which are already being compared before..Hence avoid those whose vertex is defined and compare is also true
                if(vertex_flag[vertex]){
                    cout<<"Already compared and hence leaving it"<<endl;
                    continue;
                    }
                else if (((mesh.vertex_colors_[mesh.triangles_[face][vertex-1]] + mesh.vertex_colors_[mesh.triangles_[face][vertex]]) * 0.5).isApprox(mesh.vertex_colors_[mesh.triangles_[face][vertex-1]])){
                        if(vertex_flag[vertex]){
                            //already there is a sublist for the corresponding vertex
                            vertex_index[vertex-1]->insert(vertex_index[vertex]->begin(),vertex_index[vertex]->end());
                            vertex_index[vertex]->clear(); //clearing the set
                            cout<<"Color is same : "<< mesh.triangles_[face][vertex-1] <<" Vs "<<mesh.triangles_[face][vertex]<<endl;
                            getchar();
                            no_of_null_set++;
                            // output.sort();
                            // output.erase(output.begin()); //remove the null set
                            vertex_index[vertex] = vertex_index[vertex-1]; //updating the index for next comparison if any
                            compare_flag[vertex] = false;
                            // cout<<"match and vertex present"<<endl;
                        }
                        else{
                            set<int> new_sub;
                            new_sub.insert(mesh.triangles_[face][vertex]);
                            output.push_back(new_sub);
                        }
                    }
                // Add new element if vertex is not already present
                else if(!vertex_flag[vertex]){
                        set<int> new_sub;
                        new_sub.insert(mesh.triangles_[face][vertex]);
                        output.push_back(new_sub);
                }
                // else{
                //     if(vertex_flag[vertex])
                //         continue;
                //     else{
                //         set<int> new_sub;
                //         new_sub.insert(mesh.triangles_[face][vertex]);
                //         output.push_back(new_sub);
                //     }
                // }
            }
            cout<<"Out of color comp loop"<<endl;
        cout << "vertex_flag: [ " << vertex_flag[0] << " " << vertex_flag[1] << " " << vertex_flag[2] << " ]"<<endl;
        cout << "compare_flag: [ " << compare_flag[0] << " " << compare_flag[1] << " " << compare_flag[2] << " ]"<<endl;
        cout<<"No of color comparison: "<<no_color_comp-prev_color_comp<<endl;
        prev_color_comp = no_color_comp;
            print_output();
            getchar();
        }
            //Compare 0th and 2nd element if 1st element is not equal to 0th element
            if(compare_flag[1] && compare_flag[2]){
                //ie 111 or 100..this requires comparison of 0th and 2nd element also
                cout<<"_11 case comparison"<<endl;
                getchar();
                // no_color_comp++;
                if (((mesh.vertex_colors_[mesh.triangles_[face][0]] + mesh.vertex_colors_[mesh.triangles_[face][2]]) * 0.5).isApprox(mesh.vertex_colors_[mesh.triangles_[face][0]])){
                    if(vertex_flag[2]){
                        //already there is a sublist for the corresponding vertex
                        vertex_index[0]->insert(vertex_index[2]->begin(),vertex_index[2]->end());
                        vertex_index[2]->clear();
                        // cout<<"match and vertex present"<<endl;
                    }
                    else{
                        set<int> new_sub;
                        new_sub.insert(mesh.triangles_[face][2]);
                        output.push_back(new_sub);
                    }
                }
                else{
                    if(vertex_flag[2])
                        continue;
                    else{
                        set<int> new_sub;
                        new_sub.insert(mesh.triangles_[face][2]);
                        output.push_back(new_sub);
                    }
                }
            }
            // print_output();

    ////////////////////////////////////////////////END OF COLOUR COMPARISION

        // }

//Output of OUTPUT
        // cout<<"VERTEX OUTPUT"<<endl;
        // output.sort();
        // cout << "vertex_flag: [ " << vertex_flag[0] << " " << vertex_flag[1] << " " << vertex_flag[2] << " ]"<<endl;
        // cout << "compare_flag: [ " << compare_flag[0] << " " << compare_flag[1] << " " << compare_flag[2] << " ]"<<endl;
        // cout<<"No of color comparison : "<<no_color_comp-prev_color_comp<<endl;
        // print_output();
        
        // prev_color_comp = no_color_comp;

    }
    cout<<"Total No of color comparison : "<<no_color_comp<<endl;
    cout<<"FINAL OUTPUT"<<endl;
    output.sort();
    list<set<int> >::iterator red_itr = output.begin();
    advance(red_itr,no_of_null_set); // advancing the itr to the null_set 
    output.erase(output.begin(),red_itr); //removing all the null sets
    print_output();

/////////////////////////////////END OF FACE COMARISON 


    return 0;
}