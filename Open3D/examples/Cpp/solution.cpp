#include "Open3D/Open3D.h"
#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <list>

using namespace std;

int main()
{

	open3d::geometry::TriangleMesh mesh;

	set<int>::iterator sub_itr;
    list<set<int>>::iterator out_itr;
    fstream st;

    // Reading the mesh file
    if (open3d::io::ReadTriangleMesh("../../../examples/TestData/assignment_test/test_mesh.ply", mesh)) {
        open3d::utility::LogInfo("Successfully read {}\n", "test_mesh.ply");
    } else {
        open3d::utility::LogError("Failed to read {}\n\n", "test_mesh.ply");
        return 1;
    }

    auto output = mesh.IdenticallyColoredConnectedComponents();

    //Output as specified
    st.open("results.txt",ios::out); // results.txt in the same directory
    if(!st){
    	cout<< "File Creation Failed";
    	return 1;
    }

    auto last_out_ele = output.end();
    last_out_ele--;

    cout<<"[";
    for(out_itr=output.begin();out_itr!=output.end();out_itr++){
        cout<<"[";
        auto last_in_ele = out_itr->end();
        last_in_ele--;
        for (sub_itr = out_itr->begin(); sub_itr != out_itr->end(); sub_itr++){
        	if(sub_itr != last_in_ele){
            	cout << *sub_itr<< ", ";
            	st<<to_string(*sub_itr);
            	st<<" ";
        	}
           	else{
           		cout<<*sub_itr;
           		st<<to_string(*sub_itr);
           	}
        }
        if(out_itr != last_out_ele){
        	cout <<"], ";
        	st<<"\n";
        }
       	else
       		cout <<"]";
    }
    cout<<"]"<<endl;

    st.close();
}
