import open3d as o3d

mesh = o3d.io.read_triangle_mesh("../../../examples/TestData/assignment_test/test_mesh.ply")
print(mesh)

connected_components = mesh.identically_colored_connected_components()
print(connected_components)
