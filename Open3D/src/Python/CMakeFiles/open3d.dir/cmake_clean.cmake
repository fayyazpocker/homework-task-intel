file(REMOVE_RECURSE
  "CMakeFiles/open3d.dir/geometry/lineset.cpp.o"
  "CMakeFiles/open3d.dir/geometry/octree.cpp.o"
  "CMakeFiles/open3d.dir/geometry/voxelgrid.cpp.o"
  "CMakeFiles/open3d.dir/geometry/halfedgetrianglemesh.cpp.o"
  "CMakeFiles/open3d.dir/geometry/geometry.cpp.o"
  "CMakeFiles/open3d.dir/geometry/pointcloud.cpp.o"
  "CMakeFiles/open3d.dir/geometry/trianglemesh.cpp.o"
  "CMakeFiles/open3d.dir/geometry/kdtreeflann.cpp.o"
  "CMakeFiles/open3d.dir/geometry/image.cpp.o"
  "CMakeFiles/open3d.dir/utility/eigen.cpp.o"
  "CMakeFiles/open3d.dir/utility/console.cpp.o"
  "CMakeFiles/open3d.dir/utility/utility.cpp.o"
  "CMakeFiles/open3d.dir/odometry/odometry.cpp.o"
  "CMakeFiles/open3d.dir/color_map/color_map.cpp.o"
  "CMakeFiles/open3d.dir/open3d_pybind.cpp.o"
  "CMakeFiles/open3d.dir/io/io.cpp.o"
  "CMakeFiles/open3d.dir/registration/global_optimization.cpp.o"
  "CMakeFiles/open3d.dir/registration/feature.cpp.o"
  "CMakeFiles/open3d.dir/registration/registration.cpp.o"
  "CMakeFiles/open3d.dir/docstring.cpp.o"
  "CMakeFiles/open3d.dir/camera/camera.cpp.o"
  "CMakeFiles/open3d.dir/integration/integration.cpp.o"
  "CMakeFiles/open3d.dir/visualization/visualizer.cpp.o"
  "CMakeFiles/open3d.dir/visualization/renderoption.cpp.o"
  "CMakeFiles/open3d.dir/visualization/viewcontrol.cpp.o"
  "CMakeFiles/open3d.dir/visualization/utility.cpp.o"
  "CMakeFiles/open3d.dir/visualization/visualization.cpp.o"
  "../../lib/Python/open3d.pdb"
  "../../lib/Python/open3d.cpython-37m-x86_64-linux-gnu.so"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/open3d.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
