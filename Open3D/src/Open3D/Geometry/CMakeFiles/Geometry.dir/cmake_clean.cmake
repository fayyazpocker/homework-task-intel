file(REMOVE_RECURSE
  "CMakeFiles/Geometry.dir/IntersectionTest.cpp.o"
  "CMakeFiles/Geometry.dir/TriangleMeshSimplification.cpp.o"
  "CMakeFiles/Geometry.dir/PointCloud.cpp.o"
  "CMakeFiles/Geometry.dir/PointCloudCluster.cpp.o"
  "CMakeFiles/Geometry.dir/EstimateNormals.cpp.o"
  "CMakeFiles/Geometry.dir/VoxelGrid.cpp.o"
  "CMakeFiles/Geometry.dir/LineSetFactory.cpp.o"
  "CMakeFiles/Geometry.dir/KDTreeFlann.cpp.o"
  "CMakeFiles/Geometry.dir/RGBDImageFactory.cpp.o"
  "CMakeFiles/Geometry.dir/TriangleMeshFactory.cpp.o"
  "CMakeFiles/Geometry.dir/TriangleMesh.cpp.o"
  "CMakeFiles/Geometry.dir/ImageFactory.cpp.o"
  "CMakeFiles/Geometry.dir/RGBDImage.cpp.o"
  "CMakeFiles/Geometry.dir/LineSet.cpp.o"
  "CMakeFiles/Geometry.dir/Image.cpp.o"
  "CMakeFiles/Geometry.dir/PointCloudFactory.cpp.o"
  "CMakeFiles/Geometry.dir/Octree.cpp.o"
  "CMakeFiles/Geometry.dir/Qhull.cpp.o"
  "CMakeFiles/Geometry.dir/HalfEdgeTriangleMesh.cpp.o"
  "CMakeFiles/Geometry.dir/DownSample.cpp.o"
  "CMakeFiles/Geometry.dir/VoxelGridFactory.cpp.o"
  "CMakeFiles/Geometry.dir/TriangleMeshSubdivide.cpp.o"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/Geometry.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
