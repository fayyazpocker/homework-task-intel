# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/fayyaz/intel/recurse/Open3D/3rdparty/liblzf/lzf_c.c" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/__/__/__/3rdparty/liblzf/lzf_c.c.o"
  "/home/fayyaz/intel/recurse/Open3D/3rdparty/liblzf/lzf_d.c" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/__/__/__/3rdparty/liblzf/lzf_d.c.o"
  "/home/fayyaz/intel/recurse/Open3D/3rdparty/rply/rply.c" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/__/__/__/3rdparty/rply/rply.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "UNIX"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "src"
  "3rdparty"
  "3rdparty/Eigen"
  "3rdparty/flann"
  "3rdparty/flann/algorithms"
  "3rdparty/flann/nn"
  "3rdparty/flann/util"
  "3rdparty/glew/include"
  "3rdparty/GLFW/include"
  "/usr/include/jsoncpp"
  "3rdparty/liblzf"
  "3rdparty/tomasakeninemoeller"
  "3rdparty/libpng"
  "3rdparty/rply"
  "3rdparty/rply/etc"
  "3rdparty/rply/manual"
  "3rdparty/tinyfiledialogs"
  "3rdparty/tinygltf"
  "3rdparty/tinygltf/cmake"
  "3rdparty/tinygltf/deps"
  "3rdparty/tinygltf/examples"
  "3rdparty/tinygltf/examples/basic"
  "3rdparty/tinygltf/examples/basic/basic"
  "3rdparty/tinygltf/examples/common"
  "3rdparty/tinygltf/examples/common/OpenGLWindow"
  "3rdparty/tinygltf/examples/common/ThirdPartyLibs"
  "3rdparty/tinygltf/examples/common/ThirdPartyLibs/Glew"
  "3rdparty/tinygltf/examples/common/ThirdPartyLibs/Glew/CustomGL"
  "3rdparty/tinygltf/examples/common/glm"
  "3rdparty/tinygltf/examples/common/glm/cmake"
  "3rdparty/tinygltf/examples/common/glm/doc"
  "3rdparty/tinygltf/examples/common/glm/doc/api"
  "3rdparty/tinygltf/examples/common/glm/doc/api/search"
  "3rdparty/tinygltf/examples/common/glm/doc/manual"
  "3rdparty/tinygltf/examples/common/glm/doc/theme"
  "3rdparty/tinygltf/examples/common/glm/glm"
  "3rdparty/tinygltf/examples/common/glm/glm/detail"
  "3rdparty/tinygltf/examples/common/glm/glm/gtc"
  "3rdparty/tinygltf/examples/common/glm/glm/gtx"
  "3rdparty/tinygltf/examples/common/glm/glm/simd"
  "3rdparty/tinygltf/examples/common/glm/test"
  "3rdparty/tinygltf/examples/common/glm/test/bug"
  "3rdparty/tinygltf/examples/common/glm/test/core"
  "3rdparty/tinygltf/examples/common/glm/test/external"
  "3rdparty/tinygltf/examples/common/glm/test/external/gli"
  "3rdparty/tinygltf/examples/common/glm/test/external/gli/core"
  "3rdparty/tinygltf/examples/common/glm/test/gtc"
  "3rdparty/tinygltf/examples/common/glm/test/gtx"
  "3rdparty/tinygltf/examples/common/glm/util"
  "3rdparty/tinygltf/examples/common/glm/util/conan-package"
  "3rdparty/tinygltf/examples/common/glm/util/conan-package/lib_licenses"
  "3rdparty/tinygltf/examples/common/glm/util/conan-package/test_package"
  "3rdparty/tinygltf/examples/common/imgui"
  "3rdparty/tinygltf/examples/common/nativefiledialog"
  "3rdparty/tinygltf/examples/common/nativefiledialog/screens"
  "3rdparty/tinygltf/examples/common/nativefiledialog/src"
  "3rdparty/tinygltf/examples/common/nativefiledialog/src/include"
  "3rdparty/tinygltf/examples/common/nativefiledialog/test"
  "3rdparty/tinygltf/examples/gltfutil"
  "3rdparty/tinygltf/examples/glview"
  "3rdparty/tinygltf/examples/glview/cmake"
  "3rdparty/tinygltf/examples/glview/glview"
  "3rdparty/tinygltf/examples/raytrace"
  "3rdparty/tinygltf/examples/raytrace/images"
  "3rdparty/tinygltf/examples/saver"
  "3rdparty/tinygltf/examples/validator"
  "3rdparty/tinygltf/examples/validator/app"
  "3rdparty/tinygltf/examples/validator/src"
  "3rdparty/tinygltf/experimental"
  "3rdparty/tinygltf/models"
  "3rdparty/tinygltf/models/BoundsChecking"
  "3rdparty/tinygltf/models/Cube"
  "3rdparty/tinygltf/models/Extensions-issue97"
  "3rdparty/tinygltf/tests"
  "3rdparty/tinygltf/tools"
  "3rdparty/tinygltf/tools/windows"
  "3rdparty/qhull/src"
  "3rdparty/fmt/include"
  "3rdparty/librealsense/include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/ClassIO/FeatureIO.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/ClassIO/FeatureIO.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/ClassIO/IJsonConvertibleIO.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/ClassIO/IJsonConvertibleIO.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/ClassIO/ImageIO.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/ClassIO/ImageIO.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/ClassIO/ImageWarpingFieldIO.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/ClassIO/ImageWarpingFieldIO.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/ClassIO/LineSetIO.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/ClassIO/LineSetIO.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/ClassIO/OctreeIO.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/ClassIO/OctreeIO.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/ClassIO/PinholeCameraTrajectoryIO.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/ClassIO/PinholeCameraTrajectoryIO.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/ClassIO/PointCloudIO.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/ClassIO/PointCloudIO.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/ClassIO/PoseGraphIO.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/ClassIO/PoseGraphIO.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/ClassIO/TriangleMeshIO.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/ClassIO/TriangleMeshIO.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/ClassIO/VoxelGridIO.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/ClassIO/VoxelGridIO.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/FileFormat/FileBIN.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/FileFormat/FileBIN.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/FileFormat/FileGLTF.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/FileFormat/FileGLTF.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/FileFormat/FileJPG.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/FileFormat/FileJPG.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/FileFormat/FileJSON.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/FileFormat/FileJSON.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/FileFormat/FileLOG.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/FileFormat/FileLOG.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/FileFormat/FileOBJ.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/FileFormat/FileOBJ.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/FileFormat/FileOFF.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/FileFormat/FileOFF.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/FileFormat/FilePCD.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/FileFormat/FilePCD.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/FileFormat/FilePLY.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/FileFormat/FilePLY.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/FileFormat/FilePNG.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/FileFormat/FilePNG.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/FileFormat/FilePTS.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/FileFormat/FilePTS.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/FileFormat/FileSTL.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/FileFormat/FileSTL.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/FileFormat/FileTUM.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/FileFormat/FileTUM.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/FileFormat/FileXYZ.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/FileFormat/FileXYZ.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/FileFormat/FileXYZN.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/FileFormat/FileXYZN.cpp.o"
  "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/FileFormat/FileXYZRGB.cpp" "/home/fayyaz/intel/recurse/Open3D/src/Open3D/IO/CMakeFiles/IO.dir/FileFormat/FileXYZRGB.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "UNIX"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src"
  "3rdparty"
  "3rdparty/Eigen"
  "3rdparty/flann"
  "3rdparty/flann/algorithms"
  "3rdparty/flann/nn"
  "3rdparty/flann/util"
  "3rdparty/glew/include"
  "3rdparty/GLFW/include"
  "/usr/include/jsoncpp"
  "3rdparty/liblzf"
  "3rdparty/tomasakeninemoeller"
  "3rdparty/libpng"
  "3rdparty/rply"
  "3rdparty/rply/etc"
  "3rdparty/rply/manual"
  "3rdparty/tinyfiledialogs"
  "3rdparty/tinygltf"
  "3rdparty/tinygltf/cmake"
  "3rdparty/tinygltf/deps"
  "3rdparty/tinygltf/examples"
  "3rdparty/tinygltf/examples/basic"
  "3rdparty/tinygltf/examples/basic/basic"
  "3rdparty/tinygltf/examples/common"
  "3rdparty/tinygltf/examples/common/OpenGLWindow"
  "3rdparty/tinygltf/examples/common/ThirdPartyLibs"
  "3rdparty/tinygltf/examples/common/ThirdPartyLibs/Glew"
  "3rdparty/tinygltf/examples/common/ThirdPartyLibs/Glew/CustomGL"
  "3rdparty/tinygltf/examples/common/glm"
  "3rdparty/tinygltf/examples/common/glm/cmake"
  "3rdparty/tinygltf/examples/common/glm/doc"
  "3rdparty/tinygltf/examples/common/glm/doc/api"
  "3rdparty/tinygltf/examples/common/glm/doc/api/search"
  "3rdparty/tinygltf/examples/common/glm/doc/manual"
  "3rdparty/tinygltf/examples/common/glm/doc/theme"
  "3rdparty/tinygltf/examples/common/glm/glm"
  "3rdparty/tinygltf/examples/common/glm/glm/detail"
  "3rdparty/tinygltf/examples/common/glm/glm/gtc"
  "3rdparty/tinygltf/examples/common/glm/glm/gtx"
  "3rdparty/tinygltf/examples/common/glm/glm/simd"
  "3rdparty/tinygltf/examples/common/glm/test"
  "3rdparty/tinygltf/examples/common/glm/test/bug"
  "3rdparty/tinygltf/examples/common/glm/test/core"
  "3rdparty/tinygltf/examples/common/glm/test/external"
  "3rdparty/tinygltf/examples/common/glm/test/external/gli"
  "3rdparty/tinygltf/examples/common/glm/test/external/gli/core"
  "3rdparty/tinygltf/examples/common/glm/test/gtc"
  "3rdparty/tinygltf/examples/common/glm/test/gtx"
  "3rdparty/tinygltf/examples/common/glm/util"
  "3rdparty/tinygltf/examples/common/glm/util/conan-package"
  "3rdparty/tinygltf/examples/common/glm/util/conan-package/lib_licenses"
  "3rdparty/tinygltf/examples/common/glm/util/conan-package/test_package"
  "3rdparty/tinygltf/examples/common/imgui"
  "3rdparty/tinygltf/examples/common/nativefiledialog"
  "3rdparty/tinygltf/examples/common/nativefiledialog/screens"
  "3rdparty/tinygltf/examples/common/nativefiledialog/src"
  "3rdparty/tinygltf/examples/common/nativefiledialog/src/include"
  "3rdparty/tinygltf/examples/common/nativefiledialog/test"
  "3rdparty/tinygltf/examples/gltfutil"
  "3rdparty/tinygltf/examples/glview"
  "3rdparty/tinygltf/examples/glview/cmake"
  "3rdparty/tinygltf/examples/glview/glview"
  "3rdparty/tinygltf/examples/raytrace"
  "3rdparty/tinygltf/examples/raytrace/images"
  "3rdparty/tinygltf/examples/saver"
  "3rdparty/tinygltf/examples/validator"
  "3rdparty/tinygltf/examples/validator/app"
  "3rdparty/tinygltf/examples/validator/src"
  "3rdparty/tinygltf/experimental"
  "3rdparty/tinygltf/models"
  "3rdparty/tinygltf/models/BoundsChecking"
  "3rdparty/tinygltf/models/Cube"
  "3rdparty/tinygltf/models/Extensions-issue97"
  "3rdparty/tinygltf/tests"
  "3rdparty/tinygltf/tools"
  "3rdparty/tinygltf/tools/windows"
  "3rdparty/qhull/src"
  "3rdparty/fmt/include"
  "3rdparty/librealsense/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
